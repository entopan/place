<?php namespace Entopancore\Place\Models;

use Model;

/**
 * Model
 */
class ItalyProvince extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title'];

    /*
     * Validation
     */
    public $rules = [
    ];

    public $fillable = [
        'title',
        'slug',
        'superficie',
        'num_residenti',
        'num_comuni',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_place_provinces';


    public $hasMany = [
        "places" => ["Entopancore\Place\Models\Italy", "key" => "provincia_id"]
    ];

    public $belongsTo = [
        "region" => ["Entopancore\Place\Models\ItalyRegion", "key" => "regione_id"]
    ];
}