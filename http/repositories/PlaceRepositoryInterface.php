<?php

/**
 * Created by PhpStorm.
 * User: entopanlab
 * Date: 05/06/17
 * Time: 9.23
 */

namespace Entopancore\Place\Http\Repositories;


interface PlaceRepositoryInterface
{

    public function italyAutocomplete($term);

    public function countries($type, $fieldOrder, $typeOrder, $items = 10, $where = null);
}