<?php

Route::group(
    [
        'prefix' => 'api/v1/public/place',
        'middleware' => [
           'api'
        ],
    ], function () {
    Route::get('italy', ['uses' => 'Entopancore\Place\Http\Controllers\PlaceController@italyAutocomplete', 'as' => 'italyAutocomplete']);
    Route::get('countries', ['uses' => 'Entopancore\Place\Http\Controllers\PlaceController@countries', 'as' => 'placeCountries']);
});


