-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: 192.168.10.211
-- Generation Time: May 10, 2018 at 12:04 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


-- --------------------------------------------------------

--
-- Table structure for table `entopancore_place_countries`
--

CREATE TABLE `entopancore_place_countries` (
  `id` int(11) UNSIGNED NOT NULL,
  `iso` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `iso3` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `entopancore_place_italy`
--

CREATE TABLE `entopancore_place_italy` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regione_id` int(10) UNSIGNED DEFAULT NULL,
  `provincia_id` int(11) UNSIGNED DEFAULT NULL,
  `cap` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `prefisso` int(10) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entopancore_place_countries`
--
ALTER TABLE `entopancore_place_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entopancore_place_countries_id_index` (`id`);

--
-- Indexes for table `entopancore_place_italy`
--
ALTER TABLE `entopancore_place_italy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regione_id` (`regione_id`),
  ADD KEY `provincia_id` (`provincia_id`);

--
-- Indexes for table `entopancore_place_italy_regions`
--
ALTER TABLE `entopancore_place_italy_regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entopancore_place_italy_regions_id_index` (`id`);

--
-- Indexes for table `entopancore_place_provinces`
--
ALTER TABLE `entopancore_place_provinces`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entopancore_place_countries`
--
ALTER TABLE `entopancore_place_countries`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;
--
-- AUTO_INCREMENT for table `entopancore_place_italy`
--
ALTER TABLE `entopancore_place_italy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110673;
--
-- AUTO_INCREMENT for table `entopancore_place_italy_regions`
--
ALTER TABLE `entopancore_place_italy_regions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `entopancore_place_provinces`
--
ALTER TABLE `entopancore_place_provinces`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
