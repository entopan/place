<?php namespace Entopancore\Place\Models;

use Entopancore\Utility\Traits\Modelable;
use Model;
use October\Rain\Database\Traits\Sluggable;

/**
 * Model
 */
class Italy extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sluggable;
    use Modelable;

    protected $slugs = [
        'slug' => ['title']
    ];
    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['title', 'regione', 'provincia', 'description'];
    /*
     * Validation
     */
    public $rules = [
    ];

    public $fillable = [
        'title',
        'slug',
        'description',
        'regione',
        'provincia',
        'provincia_sigla',
        'cap',
        'prefisso',
        'cod_fisco',
        'superficie',
        'lat',
        'lng',
        'num_residenti',
        'is_active',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_place_italy';

    public function getDescriptionAttribute()
    {
        if (isset($this->region)) {
            return $this->region->title . ' - ' . $this->cap . '';
        }
    }

    public $belongsTo = [
        "region" => ["Entopancore\Place\Models\ItalyRegion", "key" => 'regione_id'],
        "province" => ["Entopancore\Place\Models\ItalyProvince", "key" => "provincia_id"],
    ];
}