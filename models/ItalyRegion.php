<?php namespace Entopancore\Place\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;

/**
 * Model
 */
class ItalyRegion extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sluggable;
    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title'];

    protected $slugs = [
        'slug' => ['title']
    ];
    /*
     * Validation
     */
    public $rules = [
    ];

    public $fillable = [
        'title',
        'slug',
        'superficie',
        'num_residenti',
        'num_comuni',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_place_italy_regions';


    public $hasMany = [
        "places" => ["Entopancore\Place\Models\Italy", "key" => "regione_id"],
        "provinces" => ["Entopancore\Place\Models\ItalyProvince", "key" => "regione_id"]
    ];
}