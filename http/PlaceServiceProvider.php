<?php

namespace Entopancore\Place\Http;

use Illuminate\Support\ServiceProvider;

class PlaceServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind("Entopancore\Place\Http\Repositories\PlaceRepositoryInterface","Entopancore\Place\Http\Repositories\EloquentPlaceRepository");
    }

}
