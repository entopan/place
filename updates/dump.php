<?php namespace Entopancore\Place\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Dump extends Migration
{
    public function up()
    {
        $file = \File::get(plugins_path() . "/entopancore/place/updates/versions/v1.sql");
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::unprepared($file);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('entopancore_place_countries');
        Schema::dropIfExists('entopancore_place_italy');
        Schema::dropIfExists('entopancore_place_italy_regions');
        Schema::dropIfExists('entopancore_place_provinces');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}