<?php namespace Entopancore\Place;

use System\Classes\PluginBase;
use Validator;

class Plugin extends PluginBase
{
    public function registerSettings()
    {
    }


    public function register()
    {
    }


    public function boot()
    {
        \App::register('Entopancore\Place\Http\PlaceServiceProvider');
    }


    public function registerComponents()
    {
        return [
        ];
    }

    public function registerFormWidgets()
    {
        return [
        ];
    }
}
