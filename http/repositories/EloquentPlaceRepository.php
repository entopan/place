<?php

namespace Entopancore\Place\Http\Repositories;

use Entopancore\Place\Models\Italy;
use Entopancore\Place\Models\Country;
use Entopancore\Place\Resources\CountryResource;

class EloquentPlaceRepository implements PlaceRepositoryInterface
{

    public function italyAutocomplete($term)
    {
        return Italy::autocomplete("title", $term, "begin", null, "*", 20);
    }

    public function countries($type, $fieldOrder, $typeOrder, $items = 10, $where = null)
    {
        $countries = new Country();
        if ($where) {
            $countries = $countries->whereRaw($where);
        }
        $countries = $countries->orderBy($fieldOrder, $typeOrder)->get();
        return CountryResource::collection($countries);
    }
}