<?php namespace Entopancore\Place\Http\Controllers;

use Entopancore\Place\Http\Repositories\PlaceRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PlaceController extends Controller
{
    public $request;
    public $repository;

    public function __construct(PlaceRepositoryInterface $repository, Request $request)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    /**
     * @SWG\Get(
     *   path="/public/place/italy",
     *   tags={"Place"},
     *   summary="Get italy autocomplete",
     *   @SWG\Parameter(
     *     name="term",
     *     in="query",
     *     description="term",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function italyAutocomplete()
    {
        if (get("term")) {
            $result = $this->repository->italyAutocomplete(get("term")
            );
            return getSuccessResult($result);
        }
    }

    /**
     * @SWG\Get(
     *   path="/public/place/countries",
     *   tags={"Place"},
     *   summary="Get countries",
     *   @SWG\Parameter(
     *     name="api-order-field",
     *     in="header",
     *     description="Api order field",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="api-order-type",
     *     in="header",
     *     description="Api order type",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function countries()
    {

        $countries = $this->repository->countries(
            $this->request->header("api-type"),
            $this->request->header("api-order-field"),
            $this->request->header("api-order-type"),
            $this->request->header("api-take"),
            $this->request->header("api-where"),
            $this->request->header("api-order-type")
        );

        return getSuccessResult($countries);

    }

}