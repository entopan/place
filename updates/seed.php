<?php namespace Entopancore\Place\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Seed extends Migration
{
    public function up()
    {
        $file = \File::get(plugins_path() . "/entopancore/place/updates/versions/v2.sql");
        \DB::unprepared($file);
    }

    public function down()
    {
    }
}