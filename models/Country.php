<?php namespace Entopancore\Place\Models;

use Entopancore\Utility\Traits\Modelable;
use Model;

/**
 * Model
 */
class Country extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Modelable;
    /*
     * Validation
     */
    public $rules = [
    ];
    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['country'];
    public $fillable = ["*"];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_place_countries';


}