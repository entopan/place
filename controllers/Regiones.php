<?php namespace Entopancore\Place\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Regiones extends Controller
{
    public $implement = ['Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $bodyClass = 'compact-container';
    public $requiredPermissions = ['entopancore.place.superadmin',"entopancore.place.admin"];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Place', 'place', 'regions');
    }
}