<?php namespace Entopancore\Place\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Created by PhpStorm.
 * User: francescopassanti
 * Date: 26/07/18
 * Time: 18:16
 */
class CountryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'iso' => $this->iso,
            'iso3' => $this->iso3
        ];
    }
}